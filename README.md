# C Tips to Help

## Initialize You're Variables
There is nothing stopping you're variables from being garbage values, make sure to properly initialize them, or have
code that always initializes the variable.


## Understanding the Use of Double Indirection (void * ptr vs void ** ptr) 
Look at the difference between the two functions `construct_me_bad` and `construct_me_good`.  The reason
we use the double pointer here is because if we were to pass just a single directed pointer, this would copy
the original pointer and pass it into the function (Pass by Value), the purpose of this function though is to
act as a constructor and modify our pointer to point to the object we want constructed.  

To do this we pass the address of the pointer itself and now allow the constructor to build off of that.  The point when you will
become really really confused and frustrated is when you pair not initializing your variables with this common error.


## Understand What Memory Allocation Is
Understanding when to use you're heap vs stack is quite important.  Heap allocation is more 
costly in general,you have to never lose scope of it (or else you have a memory leak) 
and its also much more expensive to do in terms of computational cycles.  Obviously the upside is that
it isn't tied specifically to anything allowing it to be more fluid in your code.  The heap is always were you want to try
and allocate first, its fast, and it automatically is deallocated when you exit the the frame. 


Question: Why is dynamic memory allocation so expensive vs Heap allocation? Where is this memory come from? Explore your malloc call. Who provides this function?


## Preprocessing
C has a fanatastic pre processing tool, by simpling compiling with -DYOUR_FLAG_HERE, you can enable and disable features of
you're code. All modern kernels have this to allow for extra debugging features at the cost of performance. The example
in the example code is the CSDEBUG flag which will either compile in or out print statements.  This is important in the fact
that when I do not include the flag the code does not even exist as we have defined the `DEBUG` macro to compile to nothing.

## Flags
We use flags to carry state and options with us through function calls.  You'll notice that we store flags in integers, really the type is not as important
as the bit size of the type.  Each bit is an option we can turn on or off. 

```c
#define MYFLAG 0x1
uint8_t ex = 0; // This is just 0x00

// Set my flag
ex |= MYFLAG;

// Testing my flag
if (ex & MYFLAG == 1) {
// MYFLAG IS PRESENT
} else {
// MYFLAG IS NOT PRESENT
}
```


## Try and Understand Your Compiler
Your compiler can work against you, it may optimize variables out that you are specifically using for synchronization between
two threads.  A absolutely lovely resource to play around with is [Godbolt](https://www.godbolt.org).  It outputs the assembly of code given some
compiler which you choose.  You all should have done some basic assembly by this point and I think its important to understand
the general jist of whats going on under the hood.  This tool allows you to look at small sections of code, and analyze whether
your compiler is doing what you want it to do.  

This tip may be out of scope of the course, but I do believe knowing the tools that are available will make you much better
programmers in the future.

Using the -O3 option in godbolt gives a more readable form of assembly (less extra fat around the stuff you want to actually
look at).  



