#include <stdlib.h>
#include <stdio.h>

#ifdef CSDEBUG

#define DEBUG(fmt, ...) do {                        \
    printf("(%s: Line %d) ", __FILE__, __LINE__);  \
    printf(fmt, ##__VA_ARGS__);                     \
    } while(0)

#else

#define DEBUG(fmt, ...)

#endif

struct foo {
    char chars[128];
};

int
construct_me_bad(struct foo * p)
{
    p = (struct foo *)malloc(sizeof(struct foo));
    DEBUG("Pointer inside bad constructor - %p\n", p);
    return 0;
}

int
construct_me_good(struct foo ** p)
{
    *p = (struct foo *)malloc(sizeof(struct foo));
    return 0;
}

int 
main()
{
    struct foo * var = NULL;

    DEBUG("Pointer before - %p\n", var);
    construct_me_bad(var);
    DEBUG("Pointer outside bad constructor - %p\n", var);
    construct_me_good(&var);
    DEBUG("Pointer after good - %p\n", var);
    return 0;
}
