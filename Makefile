CC=gcc
DEBUGFLAGS=-g -Og -DCSDEBUG
CFLAGS=-O2

example: example.c
	$(CC) -o example example.c $(CFLAGS)

exdebug: example.c
	$(CC) -o exdebug example.c $(DEBUGFLAGS)

clean:
	rm -f example exdebug

